package shander.testapp.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shander.testapp.App;
import shander.testapp.R;
import shander.testapp.adapters.UserAdapter;
import shander.testapp.entities.User;
import shander.testapp.ui.userDetails.UserDetailsActivity;

public class MainActivity extends AppCompatActivity {

    private UserAdapter adapter;
    private ListView listView;
    private View pbLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lv_users_list);
        pbLay = findViewById(R.id.pb_users_list);
        adapter = new UserAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, UserDetailsActivity.class);
                intent.putExtra("user", (User)adapter.getItem(i));
                startActivity(intent);
            }
        });
        App.from(this).getApi().getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                adapter.setItems(response.body());
                pbLay.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }
}
