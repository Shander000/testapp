package shander.testapp.ui.userDetails;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shander.testapp.App;
import shander.testapp.R;
import shander.testapp.adapters.PostsAdapter;
import shander.testapp.entities.Post;

public class PostsFragment extends Fragment {

    private int userId;
    private PostsAdapter adapter;
    private ListView lv;
    private View pbLay;
    private FloatingActionButton btnAdd;
    private String title = "";
    private String body = "";

    public PostsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getInt("userId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_posts, container, false);
        lv = parent.findViewById(R.id.lv_posts_list);
        pbLay = parent.findViewById(R.id.pb_posts_list);
        adapter = new PostsAdapter(getActivity());
        lv.setAdapter(adapter);
        loadPosts();
        btnAdd = parent.findViewById(R.id.fab_add_post);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                View promptView = layoutInflater.inflate(R.layout.dialog_add_post, null);
                final EditText etTitle = promptView.findViewById(R.id.et_title);
                final EditText etBody = promptView.findViewById(R.id.et_body);
                etTitle.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        title = charSequence.toString();
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                etBody.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        body = charSequence.toString();
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                dialog.setNegativeButton(getActivity().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialog.setPositiveButton(getActivity().getResources().getString(R.string.send), null);
                dialog.setTitle(getActivity().getResources().getString(R.string.new_post));
                dialog.setView(promptView);
                final AlertDialog alertDialog = dialog.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(final DialogInterface dialog) {

                        Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        b.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (title.equals("")) {
                                    etTitle.setHintTextColor(Color.RED);
                                    etTitle.setHint(getActivity().getResources().getString(R.string.err_empty_title));
                                } else if (body.equals("")) {
                                    etBody.setHintTextColor(Color.RED);
                                    etBody.setHint(getActivity().getResources().getString(R.string.err_empty_body));
                                } else {
                                    pbLay.setVisibility(View.VISIBLE);
                                    lv.setVisibility(View.GONE);
                                    Post newPost = new Post();
                                    newPost.setBody(body);
                                    newPost.setTitle(title);
                                    newPost.setUserId(userId);
                                    App.from(getActivity()).getApi().sendPost(newPost).enqueue(new Callback<Post>() {
                                        @Override
                                        public void onResponse(Call<Post> call, Response<Post> response) {
                                            if (response.body() != null) {
                                                loadPosts();
                                                dialog.dismiss();
                                            } else {
                                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Post> call, Throwable t) {
                                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });
        return parent;
    }

    private void loadPosts() {
        App.from(getActivity()).getApi().getUserPosts(userId).enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                adapter.setItems(response.body());
                pbLay.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

}
