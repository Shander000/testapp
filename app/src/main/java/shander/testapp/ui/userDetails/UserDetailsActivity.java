package shander.testapp.ui.userDetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import shander.testapp.R;
import shander.testapp.entities.User;
import shander.testapp.utils.AddressConvertor;

public class UserDetailsActivity extends AppCompatActivity {

    private Switch fragmentSwitch;
    private AlbumsFragment albumsFragment;
    private PostsFragment postsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        TextView tvUserName = (TextView) findViewById(R.id.tv_user_name);
        TextView tvUserMail = (TextView) findViewById(R.id.tv_user_email);
        TextView tvUserAddress = (TextView) findViewById(R.id.tv_user_address);
        TextView tvUserPhone = (TextView) findViewById(R.id.tv_user_phone);
        final TextView tvSwitchState = (TextView) findViewById(R.id.tv_switch_state);
        fragmentSwitch = (Switch) findViewById(R.id.fragment_switch);
        User user = (User) getIntent().getSerializableExtra("user");
        tvUserName.setText(String.format("Name: %s", user.getName()));
        tvUserMail.setText(String.format("Email: %s", user.getEmail()));
        AddressConvertor convertor = new AddressConvertor();
        tvUserAddress.setText(String.format("Address: %s", convertor.getAddressText(user.getAddress())));
        tvUserPhone.setText(String.format("Phone: %s", user.getPhone()));
        albumsFragment = new AlbumsFragment();
        postsFragment = new PostsFragment();
        Bundle b = new Bundle();
        b.putInt("userId", user.getId());
        albumsFragment.setArguments(b);
        postsFragment.setArguments(b);
        fragmentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tvSwitchState.setText(!b ? "Switch to albums" : "Switch to posts");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_holder, !b ? postsFragment : albumsFragment).commit();
            }
        });
        fragmentSwitch.setChecked(true);
    }
}
