package shander.testapp.ui.userDetails;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shander.testapp.App;
import shander.testapp.R;
import shander.testapp.adapters.AlbumsAdapter;
import shander.testapp.entities.Album;
import shander.testapp.ui.AlbumDetailsActivity;

public class AlbumsFragment extends Fragment {

    private ListView lv;
    private View pbLay;
    private AlbumsAdapter adapter;
    private int userId;

    public AlbumsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getInt("userId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_album, container, false);
        lv = parent.findViewById(R.id.lv_albums_list);
        pbLay = parent.findViewById(R.id.pb_albums_list);
        adapter = new AlbumsAdapter(getActivity());
        lv.setAdapter(adapter);
        App.from(getActivity()).getApi().getUserAlbums(userId).enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                adapter.setItems(response.body());
                pbLay.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), AlbumDetailsActivity.class);
                intent.putExtra("albumId", (int)adapter.getItemId(i));
                startActivity(intent);
            }
        });
        return parent;
    }

}
