package shander.testapp.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shander.testapp.App;
import shander.testapp.R;
import shander.testapp.adapters.PhotosAdapter;
import shander.testapp.entities.Photo;

public class AlbumDetailsActivity extends AppCompatActivity {

    private ListView lv;
    private View pbView;
    private PhotosAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_details);
        int albumId = getIntent().getIntExtra("albumId", 0);
        lv = findViewById(R.id.lv_photos);
        pbView = findViewById(R.id.pb_photos);
        adapter = new PhotosAdapter(this);
        lv.setAdapter(adapter);
        App.from(this).getApi().getAlbumPhoto(albumId).enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                adapter.setItems(response.body());
                pbView.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(AlbumDetailsActivity.this, getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }
}
