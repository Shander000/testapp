package shander.testapp.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import shander.testapp.entities.Album;
import shander.testapp.entities.Photo;
import shander.testapp.entities.Post;
import shander.testapp.entities.User;

public interface ServerApi {

    @GET("/users")
    Call<List<User>> getUsers();

    @GET("/albums")
    Call<List<Album>> getUserAlbums(@Query("userId") int id);

    @GET("/posts")
    Call<List<Post>> getUserPosts(@Query("userId") int id);

    @GET("/photos")
    Call<List<Photo>> getAlbumPhoto(@Query("albumId") int albId);

    @POST("/posts")
    Call<Post> sendPost (@Body Post post);
}
