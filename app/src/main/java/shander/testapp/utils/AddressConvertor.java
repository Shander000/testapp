package shander.testapp.utils;

import shander.testapp.entities.UserAddress;

public class AddressConvertor {

    public String getAddressText (UserAddress address) {
        return String.format("%s, %s, %s", address.getCity(), address.getStreet(), address.getSuite());
    }
}