package shander.testapp;

import android.app.Application;
import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import shander.testapp.interfaces.IContract;
import shander.testapp.interfaces.ServerApi;

public class App extends Application implements IContract{

    private Retrofit retrofit;
    private ServerApi api;

    public static App from(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(ServerApi.class);
    }

    public ServerApi getApi() {
        return api;
    }
}