package shander.testapp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import shander.testapp.R;
import shander.testapp.entities.Photo;

public class PhotosAdapter extends BasicAdapter {


    public PhotosAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.photos_list_item, null);
        ImageView ivPhoto = view.findViewById(R.id.iv_photo);
        TextView tvTitle = view.findViewById(R.id.tv_photo_title);
        Photo current = (Photo) items.get(i);
        Glide.with(context).load(current.getUrl()).into(ivPhoto);
        tvTitle.setText(current.getTitle());
        return view;
    }
}
