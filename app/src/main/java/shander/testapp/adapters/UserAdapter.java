package shander.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import shander.testapp.R;
import shander.testapp.entities.User;
import shander.testapp.utils.AddressConvertor;

public class UserAdapter extends BasicAdapter {

    public UserAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.user_list_item, null);
        User user = (User) items.get(i);
        AddressConvertor c = new AddressConvertor();
        String text = String.format("%s; %s; %s", user.getName(), user.getEmail(), c.getAddressText(user.getAddress()));
        ((TextView) view.findViewById(R.id.tvUser)).setText(text);
        return view;
    }

    public User getUser(int position) {
        return (User) items.get(position);
    }
}
