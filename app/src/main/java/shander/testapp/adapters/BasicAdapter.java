package shander.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;


public abstract class BasicAdapter extends BaseAdapter {

    List<?> items = new ArrayList<>();
    LayoutInflater inflater;
    Context context;

    public void setItems(List<?> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}
