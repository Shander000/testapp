package shander.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import shander.testapp.R;
import shander.testapp.entities.Post;

public class PostsAdapter extends BasicAdapter {

    public PostsAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public long getItemId(int i) {
        return ((Post)(items.get(i))).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.posts_list_item, null);
        Post current = (Post) items.get(i);
        ((TextView) view.findViewById(R.id.post_title)).setText(current.getTitle());
        ((TextView) view.findViewById(R.id.post_body)).setText(current.getBody());
        return view;
    }
}
