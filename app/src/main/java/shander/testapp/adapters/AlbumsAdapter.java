package shander.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import shander.testapp.R;
import shander.testapp.entities.Album;

public class AlbumsAdapter extends BasicAdapter {

    public AlbumsAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int i) {
        return ((Album)(items.get(i))).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.user_list_item, null);
        ((TextView) view.findViewById(R.id.tvUser)).setText(((Album)items.get(i)).getTitle());
        return view;
    }
}
